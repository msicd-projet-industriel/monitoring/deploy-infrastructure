# Déploiement de l'infrastructure du monitoring à partir d'un pipeline sur Gitlab

__École__ : IMT Atlantique  
__Formation__ : Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__ : 2022-2023  
__UE__ : Projet industriel  
__Dépôt Gitlab__ : <https://gitlab.imt-atlantique.fr/projet-industriel/monitoring/deploy-infrastructure>  
__Date__ : 27 Mars 2023  

## Description

Ce projet permet de déployer une infrastructure Cloud sur Openstack grâce à Terraform pour assurer le monitoring de l'application ICD Meteo.  

Cette infrastructure est constituée d'un réseau, un sous-réseau, un routeur, des groupes de sécurité, des IP flottantes, une instance bastion et six noeuds.  

Le code d'infrastructure est organisé en modules réutilisable.  

Les ressources à provisionner sont variabilisées pour permettre la personnalisation de l'infrastructure.  

Le TF State de l'infrastructure est stocké et géré dans le Backend Gitlab State du projet  

## Caractéristiques de l'infrastructure

* la bastion et les six noeuds dont un manager et deux workers communiquent dans le réseau privé en full TCP et UDP.  
* la bastion administre et configure les noeuds de l'infrastructure à partir d'une machine hôte extérieure ou d'une chaine de CD.  
* toutes les instances partagent un fichier etc/hosts commum constitué des noms et adresses IP privées de tous les noeuds.  
* la bastion a un accès en SSH à tous les noeuds.  
* la bastion est accessible en SSH à partir d'internet.  
* l'un des worker est accessible en HTTP à partir d'internet.  

## Utilisation du dépôt pour déployer l'infrastructure

## Prérequis

* Un compte Openstack avec un quota suffisant de ressources  
* Une paire de clé SSH existante sur Openstack  
* Un compte sur une instance Gitlab  
* Un token d'accès à l'api du "projet-industriel" des droits "owner" pour assurer la gestion du TF State et du package registry de Gitlab  

## Configurer l'infrastructure

* Modifier le fichier __main.auto.tfvars__ pour personnaliser les ressources de l'infrastructure à provisionner sur Openstack. Les valeurs existances sont fiables.  

## Configurer l'environnement du pipeline sur Gitlab

### Créer ou mettre à jour les variables Gitlab du projet "deploy-infrastructure"

* Se rendre dans l'onglet "Settings > CI/CD" du projet.  
* Dans la section "Variables", modifier/ajouter les variables suivantes:  
  * __TF_STATE_NAME__: nom du fichier TF State.  
  * __OPENRC_SH__: contenu du fichier OpenRC sans le password (Utiliser une variable de type "File")  
  * __OPENRC_PASS__: mot de passe Openstack  

### Créer ou mettre à jour les variables Gitlab du groupe "monitoring-logging"

* Se rendre dans l'onglet "Settings > CI/CD" du groupe.  
* Dans la section "Variables", modifier/ajouter les variables suivantes:  
  * __TF_PKG_NAME__: nom du package registry pour la publication des fichiers d'inventaire générés par Terraform.  
  * __TF_PKG_VERSION__: version du package registry
  * __PRIVATE_KEY__: contenu de la clé privée SSH (Utiliser une variable de type "File")  
  * __PRIVATE_KEY_NAME__: nom de la paire de clé SSH sur Openstack  
  * __BASTION_PUBLIC_IP__: adresse IP publique de l'instance bastion  
  * __NODE_PUBLIC_IP__: adresse IP publique du noeud manager  

> Remarque: Pendant la création des variables __BASTION_PUBLIC_IP__ et __NODE_PUBLIC_IP__, il faut leur affecter des valeurs aléatoires. Elles seront mises à jour pendant l'exécution du pipeline une fois que Terraform les aura créées.  

### Créer ou mettre à jour les variables Gitlab du groupe parent "projet-industriel"

* Se rendre dans l'onglet "Settings > CI/CD" du groupe.
  * __API_TOKEN__: token d'accès à l'API Gitlab du groupe avec les privilèges "owner"  

## Déployer l'infrastructure sur Openstack

* Se rendre dans l'onglet "CI/CD > pipeline" du projet.  
* Cliquer sur le bouton "Run pipeline".  
* Sur la fénètre qui s'affiche, cliquer à nouveau sur le bouton "Run pipeline" pour valider l'opération sans renseigner de variables.  

## Détruire l'infrastructure construite sur Openstack

Pour détruire l'infrastructure, lancer manuellement le job destroy sur le pipeline précédemment déclenché  

## Résultats

Suite au déploiement, l'inventaire de l'infrastructure est généré dans le package registry du projet:  

* le fichier __hosts.ini__ contient l'inventaire de l'infrastructure qui sera exploité par Ansible  
* le fichier __public_ip.ini__ contient les noms et adresses IP flottantes associées aux instances  
* le fichier __private_ip.ini__ contient les noms et adresse IP privées de tous les noeuds  

> Le TF State est systématiquement mis à jour sur le Backend du projet dans Gitlab  
