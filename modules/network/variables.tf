variable "int_network_name" {
    type = string
    description = "Nom du réseau"
    default = "internal_network"
    validation {
        condition = length(var.int_network_name) > 2
        error_message = "Le nom du réseau doit contenir plus de 2 caractères"
    }
}

variable "int_subnet_name" {
    type = string
    description = "Nom du sous-réseau"
    default = "internal_subnet"
    validation {
        condition = length(var.int_subnet_name) > 2
        error_message = "Le nom du sous-réseau doit contenir plus de 2 caractères"
    }
}

variable "int_subnet_cidr" {
    type = string
    description = "cidr du sous-réseau"
    default = "172.16.102.0/24"
    validation {
        condition = can(cidrhost(var.int_subnet_cidr, 0))
        error_message = "Le cird du sous-réseau est invalide"
    }
}

variable "int_router_name" {
    type = string
    description = "Nom du router"
    default = "internal_router"
    validation {
        condition = length(var.int_router_name) > 2
        error_message = "Le nom du routeur doit contenir plus de 2 caractères"
    }
}

variable "ext_network_name" {
    type = string
    description = "Nom du réseau externe"
    default = "external"
}

variable "dns" {
    type = list(string)
    default = []
    description = "DNS du sous-réseau"
}