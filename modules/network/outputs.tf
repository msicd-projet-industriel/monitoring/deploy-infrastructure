output "int_network" {
    value = openstack_networking_network_v2.int_network.name

    depends_on = [
        openstack_networking_network_v2.int_network
    ]
}

output "ext_network" {
    value = data.openstack_networking_network_v2.ext_network.name

    depends_on = [
        data.openstack_networking_network_v2.ext_network
    ]
}
