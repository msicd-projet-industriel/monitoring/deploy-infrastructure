resource "null_resource" "bastion_etc_hosts" {

  triggers = {
    nodes_name      = join(",", var.nodes.*.name),
    node_ip         = join(",", var.nodes.*.access_ip_v4)
  }

  connection {
    host        = var.ssh_bastion.floatip
    type        = "ssh"
    port        = var.ssh_bastion.port
    user        = var.ssh_bastion.user
    private_key = file(var.ssh_bastion.keypair_path)
    agent       = "false"
  }

  provisioner "remote-exec" {
    inline = [<<EOT
            test -f /etc/hosts.INIT || cat /etc/hosts | sudo tee /etc/hosts.INIT
            rm -rf /tmp/hosts
            %{ for node in var.nodes ~}
                echo "${node.access_ip_v4} ${node.name}" >> /tmp/hosts
            %{ endfor ~}
            echo "$(cat /tmp/hosts; cat /etc/hosts.INIT)" | sudo tee /etc/hosts
            rm -rf /tmp/hosts
            EOT
    ]
  }
}
