variable "nodes" {
  type = list(object({
    name = string
    floatip = string
    access_ip_v4 = string
    role = list(string)
  }
  ))
  default = []
}

variable "ssh_bastion" {
  type = object({
    floatip  = string
    user     = string
    keypair_path  = string
    port = number
  })
  default = {
    floatip  = "0.0.0.0"
    user = "ubuntu"
    keypair_path = "keys/demo.pem"
    port = 22
  }
}

variable "ssh_nodes" {
  type = object({
    user     = string
    keypair_path  = string
    port     = number
  })
  default = {
    user = "ubuntu"
    keypair_path = "keys/demo.pem"
    port = 22
  }
}
