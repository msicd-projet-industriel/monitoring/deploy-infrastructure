output "name" {
  value = openstack_networking_secgroup_v2.sg.name

  depends_on = [
    openstack_networking_secgroup_rule_v2.sg_rules
  ]
}
