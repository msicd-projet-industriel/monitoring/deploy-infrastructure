# Réseau privé virtuel

module "private_network" {
  source            = "./modules/network"
  int_network_name  = var.int_network.name
  int_subnet_name   = var.int_network.subnet
  int_subnet_cidr   = var.int_network.cidr
  int_router_name   = var.int_network.router
  dns               = var.int_network.dns

  ext_network_name  = var.external_network

}

# Groupes de sécurité

module "int_secgroup" {
  source            = "./modules/secgroup"
  secgroup          = var.int_secgroup
  secgroup_rules    = var.int_secgroup_rules
}

module "app_secgroup" {
  source            = "./modules/secgroup"
  secgroup          = var.app_secgroup
  secgroup_rules    = var.app_secgroup_rules
}

module "bastion_secgroup" {
  source            = "./modules/secgroup"
  secgroup          = var.bastion_secgroup
  secgroup_rules    = var.bastion_secgroup_rules
}

# Instance bastion

module "bastion" {
  source            = "./modules/instance"
  name              = "${var.bastion.name}.${var.bastion.name_suffix}"
  image_name        = var.bastion.image
  flavor_name       = var.bastion.flavor
  keypair_name      = var.keypair_name
  network_name      = module.private_network.int_network
  secgroups         = [ module.int_secgroup.name, module.bastion_secgroup.name ]
  floatip           = true
  external_network  = module.private_network.ext_network

  tags_role         = var.bastion.role

  depends_on = [
    module.private_network,
    module.bastion_secgroup
  ]
}

# Instance edge

module "edge" {
  source            = "./modules/instance"
  name              = "${var.node.name_prefix}1.${var.node.name_suffix}"
  image_name        = var.node.image
  flavor_name       = var.node.flavor
  keypair_name      = var.keypair_name
  network_name      = module.private_network.int_network
  secgroups         = [ module.int_secgroup.name, module.app_secgroup.name ]
  floatip           = true
  external_network  = module.private_network.ext_network

  tags_role         = var.worker_role

  depends_on = [
    module.private_network,
    module.app_secgroup
  ]
}

# Instances managers

module "manager" {
  source            = "./modules/instance"
  count             = var.manager_count

  name              = "${var.manager.name_prefix}${count.index + 1}.${var.manager.name_suffix}"
  image_name        = var.manager.image
  flavor_name       = var.manager.flavor
  keypair_name      = var.keypair_name
  network_name      = module.private_network.int_network
  secgroups         = [ module.int_secgroup.name ]
  floatip           = false
  external_network  = module.private_network.ext_network

  tags_role         = var.manager_role

  depends_on = [
    module.private_network,
    module.app_secgroup
  ]
}

# Instances worker

module "worker" {
  source            = "./modules/instance"
  count             = "${var.worker_count - 1}"

  name              = "${var.node.name_prefix}${count.index + 2}.${var.node.name_suffix}"
  image_name        = var.node.image
  flavor_name       = var.node.flavor
  keypair_name      = var.keypair_name
  network_name      = module.private_network.int_network
  secgroups         = [ module.int_secgroup.name ]
  floatip           = false

  tags_role         = var.worker_role

  depends_on = [
    module.private_network
  ]
}

# Copie des noms et IP internes sur toutes les instances

module "copy_etc_hosts" {
  source            = "./modules/etc-hosts"
  nodes             = concat([module.edge], module.manager, module.worker) # fusion de listes de noeuds
  
  ssh_bastion       = {
    floatip         = module.bastion.floatip
    user            = var.bastion.user
    port            = var.bastion.ssh_port
    keypair_path    = var.PRIVATE_KEY_PATH
  }

  ssh_nodes         = {
    user            = var.node.user
    port            = var.node.ssh_port
    keypair_path    = var.PRIVATE_KEY_PATH
  }

  depends_on = [
    module.bastion,
    module.edge,
    module.manager,
    module.worker
  ]
}

# Exportation des données et création de l'inventaire

module "inventory" {
  source            = "./modules/inventory"
  instances         = concat([module.bastion], [module.edge], module.manager, module.worker) # fusion des listes de toutes les instances
  
  ssh_bastion       = {
    floatip         = module.bastion.floatip
    user            = var.bastion.user
    port            = var.bastion.ssh_port
    keypair_path    = var.PRIVATE_KEY_PATH
  }

  bastion_role      = var.bastion.role
  manual_trigger    = var.manual_trigger

  depends_on = [
    module.bastion,
    module.manager,
    module.edge,
    module.worker
  ]
}
